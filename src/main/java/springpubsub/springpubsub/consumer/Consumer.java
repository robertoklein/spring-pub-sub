package springpubsub.springpubsub.consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class Consumer {

    private final String TOPIC = "mqtt-kafka-topic-di";

    private final String bootstrap;

    @Autowired
    public Consumer(@Value("${kafka.bootstrapAddress}") String bootstrap_servers) {
        this.bootstrap = bootstrap_servers;
    }

    @KafkaListener(topics = {TOPIC})
    public void listenerInsert(@Payload String message) throws Exception {


     //para enviar para o kafka, utilizar o metodo abaixo
    //sendoToKafka();
    }

//    private void sendToKafka(KafkaPayload payload, String topic) throws JsonProcessingException {
//        String payloadJson = payloadToJson(payload);
//        Producer producer = new Producer(bootstrap);
//        producer.send(topic,payloadJson);
//    }


}
